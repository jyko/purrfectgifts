/** 
 * purrfect gifts - main object
 * contains products object
 * and cart object
 */
var PurrfectGifts = {};

/* Product object */
var Products = {
    "art": {
        "meow": {
            "Name": "Meow by Pi'catso",
            "Artist": "Pi'catso",
            "Color": "Blue/Green/Black",
            "Size": "11x13",
            "Medium": "Oil on Woven Canvas",
            "Price": 3000,
            "Availability": "Yes"
        },
        "creationofpurdam": {
            "Artist": "Meowkoangelo",
            "Color": "MISC",
            "Size": "18x32",
            "Medium": "Oil on Woven Canvas",
            "Price": 7000,
            "Availability": "Yes"
            },
        "hiss": {
            "Artist": "Meowington",
            "Color": "MISC",
            "Size": "8x11",
            "Medium": "Oil on Woven Canvas",
            "Price": 5000,
            "Availability": "Yes"
        }
    },
    "toys": {
        "furfestbundle": {
            "Name": "Fur Fest Bundle",
            "Color": "Blue/Gree/Pink",
            "Size": "12 pieces",
            "Material": "Fabric/Plastic",
            "Price": 30,
            "Availability": "Yes"
        },
        "catplane":  {
            "Name": "Cat Plane",
            "Color": "Tan/Red",
            "Size": "1 pieces",
            "Material": "Cardboard/Plastic",
            "Price": 700,
            "Availability": "Yes"
        },
        "boxwings":  {
            "Name": "Box Wings",
            "Color": "Tan",
            "Size": "1 pieces",
            "Material": "Cardboard",
            "Price": 50,
            "Availability": "Yes"
        }
    },
    "jewelry": {
        "purrfectpearls":  {
            "Type":"Necklace",
            "Color": "Gold/Pearl",
            "Size": "2 pieces",
            "Material": "GOLD",
            "Price": 2000,
            "Availability": "Yes"
        },
        "egyptiangoddess":  {
            "Type":"Necklace",
            "Color": "Gold/Black",
            "Size": "2 pieces",
            "Material": "Cardboard/Plastic",
            "Price": 7500,
            "Availability": "Yes"
        },
        "thugmittens":  {
            "Type": "Bracelet",
            "Color": "Gold",
            "Size": "1 pieces",
            "Material": "GOLD",
            "Price": 5500,
            "Availability": "Yes"
        }
    },
    "purrfectgifts": {
        "cats":  {
            "Type": "Mug",
            "Color": "Black/White",
            "Size": "1 pieces",
            "Material": "Ceramin",
            "Price": 20,
            "Availability": "Yes"
        },
        "tigerbetweenthesheets":  {
            "Type": "Sheets",
            "Color": "Black/White",
            "Size": "1 pieces",
            "Material": "Cotton",
            "Price": 75,
            "Availability": "Yes"
        },
        "kittycorn":  {
            "Type": "Horn",
            "Color": "White",
            "Size": "1 pieces",
            "Material": "Plastic",
            "Price": 5,
            "Availability": "Yes"
        }
    }
}

/* cart object */
var Cart = {
    items: {},
    /* save Cart object string as a cookie */
    save: function() {
        //prepend items= and save it to cookie
        var CartJSONString = "items=" + JSON.stringify(this.items);
        document.cookie = CartJSONString;
    },
    /* get this json object from cookie */
    get: function() {
        console.log(document.cookie);
        //get cookies
        var cookies = document.cookie;
        //remove items= in the beginning of cookies
        var cartItems = cookies.replace('items=', '');
        //check to see if empty
        if(cartItems != '') {
            //convert to json and set items
            this.items = JSON.parse(cartItems);
        }
    },
    add: function(category, item, qty) {
        console.log(category, item, qty);
        if(this.items[category] === undefined) {
            this.items[category] = {};
        }

        this.items[category][item] = parseInt(qty);

        alert('Item has been added');
        this.save();
        console.log(this);
    },
    total: function() {
        var total = 0;
        for (var categories in this.items) {

            for(var items in this.items[categories]){

                total += Products[categories][items].Price * this.items[categories][items];
            }
        }

        return total;

    },
    getCart: function() {
        var string = '';
        for (var categories in this.items) {

            for(var items in this.items[categories]){
                string += '<b>' + Products[categories][items].Name + '</b><br/>';
                string += '<b>Price:</b> ' + Products[categories][items].Price + '<br/>';
                string += '<b>Qty:</b> ' + this.items[categories][items] + ' <br/><br/>';
            }
        }

        if (string === '') {
            return 'Cart is empty!';
        }

        string += '<h2>Total: $' + this.total() + '</h2>';
        return string;
    },
    clear: function() {
        this.items = {};
        //clear cookies
        document.cookie = "items={}";
    },
    getElement: function(id) {
        return document.getElementById(id).value;
    }
}